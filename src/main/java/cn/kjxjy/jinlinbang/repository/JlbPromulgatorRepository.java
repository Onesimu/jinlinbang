package cn.kjxjy.jinlinbang.repository;

import cn.kjxjy.jinlinbang.entity.JlbPromulgator;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JlbPromulgatorRepository extends JpaRepository<JlbPromulgator, Long> {
}
