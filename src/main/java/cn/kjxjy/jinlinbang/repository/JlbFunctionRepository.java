package cn.kjxjy.jinlinbang.repository;

import cn.kjxjy.jinlinbang.entity.JlbFunction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JlbFunctionRepository extends JpaRepository<JlbFunction, Long> {
}
