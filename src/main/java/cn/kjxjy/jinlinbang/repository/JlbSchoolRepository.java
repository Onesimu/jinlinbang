package cn.kjxjy.jinlinbang.repository;

import cn.kjxjy.jinlinbang.entity.JlbSchool;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JlbSchoolRepository extends JpaRepository<JlbSchool, Long> {
}
