package cn.kjxjy.jinlinbang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JinlinbangApplication {

    public static void main(String[] args) {
        SpringApplication.run(JinlinbangApplication.class, args);
    }
}
