package cn.kjxjy.jinlinbang.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class JlbPromulgator {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long proId;
    private String qd;
    private String mdd;
    private String name;
    private Long tell;
    private Long schoolId;
    private Long functionId;
    private Long girl;
    private Long speed;
    private Long weight;
    private java.sql.Timestamp createTime;


    public Long getProId() {
        return proId;
    }

    public void setProId(Long proId) {
        this.proId = proId;
    }


    public String getQd() {
        return qd;
    }

    public void setQd(String qd) {
        this.qd = qd;
    }


    public String getMdd() {
        return mdd;
    }

    public void setMdd(String mdd) {
        this.mdd = mdd;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Long getTell() {
        return tell;
    }

    public void setTell(Long tell) {
        this.tell = tell;
    }


    public Long getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Long schoolId) {
        this.schoolId = schoolId;
    }


    public Long getFunctionId() {
        return functionId;
    }

    public void setFunctionId(Long functionId) {
        this.functionId = functionId;
    }


    public Long getGirl() {
        return girl;
    }

    public void setGirl(Long girl) {
        this.girl = girl;
    }


    public Long getSpeed() {
        return speed;
    }

    public void setSpeed(Long speed) {
        this.speed = speed;
    }


    public Long getWeight() {
        return weight;
    }

    public void setWeight(Long weight) {
        this.weight = weight;
    }


    public java.sql.Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(java.sql.Timestamp createTime) {
        this.createTime = createTime;
    }

}
