var vm = new Vue({
    el: '#app',
    data: {
        jlbPromulgators: [],
        jlbFunctions: [],
        jlbSchools: []
    },
    created() {
        // const host = 'http://localhost:8088/'
        const host = 'http://' + window.location.host + '/api/'
        $.get(host + 'jlbPromulgators', res => {
            const jlbPromulgators = res._embedded.jlbPromulgators
            localStorage.jlbPromulgators = JSON.stringify(jlbPromulgators)
            this.jlbPromulgators = jlbPromulgators
        })
        $.get(host + 'jlbFunctions', res => {
            const jlbFunctions = res._embedded.jlbFunctions
            jlbFunctions.forEach(it => it.id = this.getId(it, 'jlbFunction'))
            localStorage.jlbFunctions = JSON.stringify(jlbFunctions)
            this.jlbFunctions = jlbFunctions
        })
        $.get(host + 'jlbSchools', res => {
            const jlbSchools = res._embedded.jlbSchools
            jlbSchools.forEach(it => it.id = this.getId(it, 'jlbSchool'))
            localStorage.jlbSchools = JSON.stringify(jlbSchools)
            this.jlbSchools = jlbSchools
        })
    },
    mounted() {

    },
    computed: {},
    methods: {
        functionName(id = 1) {
            const find = this.jlbFunctions.find((it, idx) => it.id == id) || this.jlbFunctions[0]
            return find && find.functionName || this.jlbFunctions[0].functionName
        },
        getId(it, name) {
            // const it = this.jlbFunctions[index];
            const href = it._links[name].href
            return href.substring(href.lastIndexOf('/') + 1)
        }
    }
})