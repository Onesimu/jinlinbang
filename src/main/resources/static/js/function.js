var host = "";
var myAjax = function(_method, _url, _async, _dataType, _data, _errorBack, _successBack) {
	$.ajax({
		type: _method,
		url: host + _url,
		async: async,
		dataType: _dataType,
		data: _data,
		error: errorBack,
		success: successBack
	});
};

var selectMessage = function (id) {
	var mask = $('.mask');
	var $id = $(id);
	var weuiActionsheet = $(id);
	$id.addClass('weui_actionsheet_toggle');
	mask.show().addClass('weui_fade_toggle').click(function() {
		hideActionSheet($id, mask);
	});
	$('.actionsheet_cancel').click(function() {
		hideActionSheet($id, mask);
	});
	$id.unbind('transitionend').unbind('webkitTransitionEnd');

	function hideActionSheet($id, mask) {
		$id.removeClass('weui_actionsheet_toggle');
		mask.removeClass('weui_fade_toggle');
		$id.on('transitionend', function() {
			mask.hide();
		}).on('webkitTransitionEnd',
			function() {
				mask.hide();
			})
	}
};

function returnMessage(id) {
//	alert($(this).text());
	$(id).text($(this).text());
//	$(id1).val($(this).text());
	$('.actionsheet_cancel').click();

}