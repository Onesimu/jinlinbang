/*
Navicat MySQL Data Transfer

Source Server         : Test
Source Server Version : 50718
Source Host           : localhost:3306
Source Database       : weixin

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2018-04-24 14:56:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for jlb_function
-- ----------------------------
DROP TABLE IF EXISTS `jlb_function`;
CREATE TABLE `jlb_function` (
  `FUNCTION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FUNCTION_NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`FUNCTION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jlb_function
-- ----------------------------
INSERT INTO `jlb_function` VALUES ('1', '帮取快递');
INSERT INTO `jlb_function` VALUES ('2', '校园快印	\r\n');
INSERT INTO `jlb_function` VALUES ('3', '同校打水');

-- ----------------------------
-- Table structure for jlb_promulgator
-- ----------------------------
DROP TABLE IF EXISTS `jlb_promulgator`;
CREATE TABLE `jlb_promulgator` (
  `PRO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `QD` varchar(255) DEFAULT NULL,
  `MDD` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `TELL` int(11) DEFAULT NULL,
  `SCHOOL_ID` int(11) DEFAULT NULL,
  `FUNCTION_ID` int(11) DEFAULT NULL,
  `GIRL` int(11) DEFAULT NULL,
  `SPEED` int(11) DEFAULT NULL,
  `WEIGHT` int(11) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`PRO_ID`),
  KEY `FK_dw2tipy9ujeva3v0pxy2vsso4` (`SCHOOL_ID`),
  KEY `FK_btg1fro9oh4e5txb6pwkxs05w` (`FUNCTION_ID`),
  CONSTRAINT `FK_btg1fro9oh4e5txb6pwkxs05w` FOREIGN KEY (`FUNCTION_ID`) REFERENCES `jlb_function` (`FUNCTION_ID`),
  CONSTRAINT `FK_dw2tipy9ujeva3v0pxy2vsso4` FOREIGN KEY (`SCHOOL_ID`) REFERENCES `jlb_school` (`SCHOOL_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jlb_promulgator
-- ----------------------------
INSERT INTO `jlb_promulgator` VALUES ('110', '科大南门', '15B320', '小明', '1234567891', '1', '1', '0', '20', '1', null);
INSERT INTO `jlb_promulgator` VALUES ('111', '科大西门', '15B314', '小张', '987654321', '1', '2', '1', '10', '5', null);
INSERT INTO `jlb_promulgator` VALUES ('112', '师大西门', '14A111', '小李', '1234567892', '2', '1', '1', '5', '2', null);
INSERT INTO `jlb_promulgator` VALUES ('113', 'afc', null, null, null, null, null, null, null, null, '2018-02-27 16:02:11');
INSERT INTO `jlb_promulgator` VALUES ('114', '', null, null, null, null, null, null, null, null, '2018-02-27 16:08:32');
INSERT INTO `jlb_promulgator` VALUES ('115', '11', null, null, null, null, null, null, null, null, '2018-03-09 16:52:23');
INSERT INTO `jlb_promulgator` VALUES ('116', '', null, null, null, null, null, null, null, null, '2018-03-09 17:05:04');
INSERT INTO `jlb_promulgator` VALUES ('117', '55555', null, null, null, null, null, null, null, null, '2018-03-11 10:55:55');
INSERT INTO `jlb_promulgator` VALUES ('118', '科大', null, null, null, null, null, null, null, null, '2018-04-09 18:52:18');

-- ----------------------------
-- Table structure for jlb_school
-- ----------------------------
DROP TABLE IF EXISTS `jlb_school`;
CREATE TABLE `jlb_school` (
  `SCHOOL_ID` int(11) NOT NULL AUTO_INCREMENT,
  `SCHOOL_NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SCHOOL_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jlb_school
-- ----------------------------
INSERT INTO `jlb_school` VALUES ('1', '河北科技');
INSERT INTO `jlb_school` VALUES ('2', '河北师大');
INSERT INTO `jlb_school` VALUES ('3', '河北经贸');
INSERT INTO `jlb_school` VALUES ('4', null);
